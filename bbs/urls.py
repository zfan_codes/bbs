"""bbs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from blog import views as blog_view
from django.conf.urls.static import static
from django.conf import settings

from app01 import urls as app01_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('reg/', blog_view.reg, name='reg'),
    path('login/', blog_view.login, name="login"),
    path('', blog_view.index, name="index"),
    path('logout/', blog_view.logout, name="logout"),
    path('pc-geetest/register', blog_view.get_geetest, name="get_geetest"),

    path('blog/<str:blog_name>',blog_view.blog_info,name='blog_info'),

    path('app01', include(app01_urls)),
    # media相关的路由设置
]
urlpatterns += static('/media/', document_root=settings.MEDIA_ROOT)