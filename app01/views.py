from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
@csrf_exempt  # 将不会效验csrf
def upload_file(request):
    if request.method == 'POST':
        file_obj = request.FILES.get('image')
        # print(file_obj,type(file_obj))
        with open(file_obj.name, 'wb') as f:
            for line in file_obj:
                f.write(line)
    return render(request, 'upload_file.html')
